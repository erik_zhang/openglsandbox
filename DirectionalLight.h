//
// Created by erzhang on 2019-08-13.
//

#ifndef OPENGLSANDBOX_DIRECTIONALLIGHT_H
#define OPENGLSANDBOX_DIRECTIONALLIGHT_H

#include "Light.h"

#include <vector>
class DirectionalLight : public Light {
public:
    DirectionalLight();
    DirectionalLight(GLfloat shadowWidth, GLfloat shadowHeight,
                    GLfloat red, GLfloat green, GLfloat blue,
                    GLfloat aIntensity, GLfloat dIntensity, GLfloat xDir,
                    GLfloat yDir, GLfloat zDir);

    glm::mat4 CalculateLightTransform();

    void UseLight(GLuint ambientIntensityLocation, GLuint ambientColourLocation, GLuint diffuseIntensityLocation, GLuint positionLocation);
    ~DirectionalLight();
private:
    glm::vec3 direction{};
};


#endif //OPE{}NGLSANDBOX_DIRECTIONALLIGHT_H

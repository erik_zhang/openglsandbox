
#ifndef OPENGLSANDBOX_TEXTURE_H
#define OPENGLSANDBOX_TEXTURE_H

#include <GL/glew.h>

#include "stb_image.h"

class Texture {
public:
    Texture();

    explicit Texture(const char *fileLoc);

    bool LoadTexture();

    bool LoadTextureA();

    void UseTexture();

    void ClearTexture();

    ~Texture();

private:
    GLuint textureID;
    int width, height, bitDepth;

    const char *fileLocation;
};

#endif // OPENGLSANDBOX_TEXTURE_H
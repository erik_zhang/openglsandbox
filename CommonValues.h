//
// Created by erzhang on 2019-08-13.
//

#ifndef OPENGLSANDBOX_COMMONVALUES_H
#define OPENGLSANDBOX_COMMONVALUES_H

const int MAX_POINT_LIGHTS = 3;
const int MAX_SPOT_LIGHTS = 3;

#endif //OPENGLSANDBOX_COMMONVALUES_H

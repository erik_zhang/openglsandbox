//
// Created by erzhang on 2019-08-08.
//

#ifndef OPENGLSANDBOX_WINDOW_H
#define OPENGLSANDBOX_WINDOW_H


#include <GL/glew.h>
#include <GLFW/glfw3.h>

class Window {
public:
    Window();

    Window(GLint windowWidth, GLint windowHeight);

    ~Window();

    int Initialise();

    GLfloat getBufferWidth() { return bufferWidth; }

    GLfloat getBufferHeight() { return bufferHeight; }

    bool getShouldClose() { return glfwWindowShouldClose(mainWindow); }

    bool *getsKeys() { return keys; }

    GLfloat getXChange();

    GLfloat getYChange();

    void swapBuffers() { glfwSwapBuffers(mainWindow); }


private:
    GLFWwindow *mainWindow;

    GLint width, height;
    GLint bufferWidth, bufferHeight;

    bool keys[1024];

    GLfloat lastX;
    GLfloat lastY;
    GLfloat xChange;
    GLfloat yChange;
    bool mouseFirstMoved = true;

    void createCallbacks();

    static void handleKeys(GLFWwindow *window, int key, int code, int action, int mode);

    static void handleMouse(GLFWwindow *window, double xPos, double yPos);
};


#endif //OPENGLSANDBOX_SHADER_H

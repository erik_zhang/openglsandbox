//
// Created by erzhang on 2019-08-12.
//

#ifndef OPENGLSANDBOX_LIGHT_H
#define OPENGLSANDBOX_LIGHT_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "ShadowMap.h"

class Light {
public:
    Light();
    Light(GLfloat shadowWidth, GLfloat shadowHeight, GLfloat red, GLfloat green, GLfloat blue, GLfloat aIntensity, GLfloat dIntensity);

    ShadowMap* GetShadowMap(){return shadowMap;}
    ~Light() = default;

protected:
    glm::vec3 colour{};
    GLfloat ambientIntensity, diffuseIntensity;

    glm::vec3 direction;

    glm::mat4 lightProj;
    ShadowMap* shadowMap;

};


#endif //OPENGLS{}ANDBOX_LIGHT_H

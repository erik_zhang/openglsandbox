//
// Created by erzhang on 2019-08-13.
//

#ifndef OPENGLSANDBOX_POINTLIGHT_H
#define OPENGLSANDBOX_POINTLIGHT_H

#include <vector>
#include "Light.h"

#include "OmniShadowMap.h"

class PointLight : public Light {
public:
    PointLight();

    ~PointLight();

    PointLight(GLuint shadowWidth, GLuint shadowHeight, GLfloat near, GLfloat far,
            GLfloat red, GLfloat green, GLfloat blue, GLfloat aIntensity, GLfloat dIntensity, GLfloat xPos,
               GLfloat yPos, GLfloat zPos, GLfloat con, GLfloat lin, GLfloat exp);


    void UseLight(GLuint ambientIntensityLocation, GLuint ambientColourLocation, GLuint diffuseIntensityLocation,
                  GLuint positionLocation, GLuint constantLocation, GLuint linearLocation, GLuint exponentLocation);

    GLfloat GetFarPlane();
    glm::vec3 GetPosition();
    std::vector<glm::mat4> CalculateLightTransform();



protected:
    glm::vec3 position;
    GLfloat constant, linear, exponent;


    GLfloat farPlane;
};


#endif //OPENGLSANDBOX_POINTLIGHT_H

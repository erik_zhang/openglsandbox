//
// Created by ezhang on 2019-08-14.
//

#ifndef OPENGLSANDBOX_SHADOWMAP_H
#define OPENGLSANDBOX_SHADOWMAP_H

#include <stdio.h>

#include <GL/glew.h>

class ShadowMap {
public:
    ShadowMap();

    virtual bool Init(GLuint width, GLuint height);

    virtual void Write();

    virtual void Read(GLenum textureUnit);

    GLuint GetShadowWidth(){ return shadowWidth;}
    GLuint GetShadowHeight() {return shadowHeight;}

    ~ShadowMap();

protected:
    GLuint FBO, shadowMap; //Frame buffer object
    GLuint shadowWidth, shadowHeight;
};


#endif //OPENGLSANDBOX_SHADOWMAP_H

//
// Created by erzhang on 2019-08-08.
//

#ifndef OPENGLSANDBOX_MESH_H
#define OPENGLSANDBOX_MESH_H

#include <GL/glew.h>

class Mesh {
public:
    Mesh();

    ~Mesh();

    void CreateMesh(GLfloat *vertices, unsigned int *indices, unsigned int numOfVertices, unsigned int numOfIndices);

    void RenderMesh();

    void ClearMesh();

private:
    GLuint VAO, VBO, IBO;
    GLsizei indexCount;
};

#endif // OPENGLSANDBOX_MESH_H
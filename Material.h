//
// Created by erzhang on 2019-08-12.
//

#ifndef OPENGLSANDBOX_MATERIAL_H
#define OPENGLSANDBOX_MATERIAL_H

#include "GL/glew.h"

class Material {
public:
    Material();
    Material(GLfloat sIntensity, GLfloat shine);
    ~Material();

    void UseMaterial(GLuint specularIntensityLocation, GLuint shininessLocation);

private:
    GLfloat specularIntensity;
    GLfloat shininess;
};


#endif //OPENGLSANDBOX_MATERIAL_H

#version 330

layout (location = 0) in vec3 pos;

uniform mat4 model;
uniform mat4 directionalLightTransform;  // (Projection*view) Light src is seens as orthogonal camera

void main(){
    gl_Position = directionalLightTransform*model*vec4(pos, 1.0);
}
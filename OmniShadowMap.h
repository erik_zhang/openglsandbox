//
// Created by ezhang on 2019-08-15.
//

#ifndef OPENGLSANDBOX_OMNISHADOWMAP_H
#define OPENGLSANDBOX_OMNISHADOWMAP_H

#include "ShadowMap.h"
class OmniShadowMap : public ShadowMap {
public:
    OmniShadowMap();

    bool Init(GLuint width, GLuint height);

    void Write();

    void Read(GLenum textureUnit);


    ~OmniShadowMap();

};


#endif //OPENGLSANDBOX_OMNISHADOWMAP_H
